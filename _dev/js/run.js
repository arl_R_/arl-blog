(function (DOC, WIN) {
    'use strict';

    const pageURL = WIN.location.pathname;
    const pageGenre = pageURL
        .replace(/^\//, '')
        .replace('article/', '')
        .replace(/\/.*/, '');

    setGrobalNav(DOC, WIN, pageGenre);
    setArticle(DOC, WIN, pageGenre);
}(document, window));