const setGrobalNav = (DOC, WIN, pageGenre) => {
    'use strict';
    const humburgerBtn = DOC.getElementById('HumburgerBtn');
    const globalNav = DOC.getElementById('GlobalNavWrapper');
    const navLink = Array.from(globalNav.getElementsByClassName('text-link'));
    const html = DOC.querySelector('html');
    const genre = pageGenre;

    const mediaPC = '(min-width: 1024px)';
    const mqlPc = WIN.matchMedia(mediaPC);

    let isRunning = false;

    const expandGlobalNav = () => {
        if (isRunning) {
            return;
        }
        isRunning = true;
        globalNav.removeAttribute('data-anime');
        html.classList.toggle('hidden');

        if (humburgerBtn.getAttribute('aria-expanded') === 'true') {
            humburgerBtn.setAttribute('aria-expanded', 'false');
            globalNav.classList.toggle('hidden');
            setTimeout(() => {
                globalNav.style.display= 'none';
            }, 600);
        } else {
            humburgerBtn.setAttribute('aria-expanded', 'true');
            globalNav.removeAttribute('style');
            setTimeout(() => {
                globalNav.classList.toggle('hidden');
            }, 1);
        }

        setTimeout(() => {
            isRunning = false;
        }, 600);
    }

    const matchMedia = () => {
        if (mqlPc.matches) {
            humburgerBtn.setAttribute('aria-expanded', 'false');
            html.classList.remove('hidden');
            globalNav.classList.remove('hidden');
            globalNav.removeAttribute('style');
            globalNav.dataset.anime = 'none';
        } else {
            globalNav.classList.add('hidden');
        }
    }

    const currentGenre = () => {
        const currentItem = navLink.find((elem) => {
            return elem.dataset.genre === genre;
        });
        if (!currentItem) {
            return;
        }
        currentItem.classList.add('selected');
    };

    humburgerBtn.addEventListener('click', expandGlobalNav);
    WIN.matchMedia(mediaPC).addListener(matchMedia);
    matchMedia();
    currentGenre();
};

const setArticle = async (DOC, WIN, pageGenre) => {
    'use strict';

    const data = await fetch('/resource/json/article.json');
    const jsonData = await data.json();

    /**
     * @type [Node] mainTarget main要素内、記事を追加するol要素 なければnull
     * @type [Node] sideTarget 右ナビ内、記事を追加するol要素
     * @type [Array] allArticle JSONから作成したそれぞれの記事を格納する配列
     * @type [String] genre URLからどのジャンルのページを開いてるかの取得
     */
    let mainTarget;
    try {
        mainTarget = DOC.getElementById('mainArtList');
    } catch(e) {
        mainTarget = null;
    }
    const sideTarget = DOC.getElementById('sideArtList');
    const allArticle = [];
    const sideArticle = [];
    const genre = pageGenre;

    class Article {
        constructor() {
            this.articleWrapper = DOC.createElement('li');
            this.articleElm = DOC.createElement('article');
            this.articleLink = DOC.createElement('a');
            this.articleHdg = DOC.createElement('h3');
            this.articleDate = DOC.createElement('time');
            this.articleGenre = DOC.createElement('span');
            this.articleGenreInner = DOC.createElement('span');
            this.articleTagList = DOC.createElement('ul');
            this.articleTagListItem = [];
            this.articleDesc = DOC.createElement('p');
            this.articleImg = DOC.createElement('img');

            this.articleWrapper.className = 'list-item';
            this.articleLink.className = 'article-link';
            this.articleHdg.className = 'hdg-03';
            this.articleDate.className = 'date';
            this.articleGenre.className = 'genre';
            this.articleGenreInner.className = 'inner';
            this.articleTagList.className = 'tag-list';
            this.articleTagListItem.className = 'list-item';
            this.articleDesc.className = 'description';
            this.articleImg.className = 'article-img';
            this.articleImg.alt = '';

            this.articleGenre.appendChild(this.articleGenreInner);
            this.articleLink.appendChild(this.articleHdg);
            this.articleLink.appendChild(this.articleDate);
            this.articleLink.appendChild(this.articleGenre);
            this.articleLink.appendChild(this.articleTagList);
            this.articleLink.appendChild(this.articleDesc);
            this.articleLink.appendChild(this.articleImg);
            this.articleElm.appendChild(this.articleLink);
            this.articleWrapper.appendChild(this.articleElm);
        }
        /**
         * constructorで作ったarticleのテンプレにJSONの情報をいれる
         * @param [Number] index JSONデータのindex番号
         */
        createArticle(index, position) {
            this.articleHdg.innerText = jsonData[index].title;
            this.articleLink.href = `/article/${jsonData[index].genre}/${jsonData[index].date}/`;
            this.articleDate.innerText = jsonData[index].date;
            this.articleDesc.innerText = jsonData[index].description;
            this.articleImg.src = `/article/${jsonData[index].genre}/${jsonData[index].date}/img/${jsonData[index].img}`;

            // ジャンルの文字列の変換
            switch (jsonData[index].genre) {
                case 'html':
                case 'css':
                    this.articleGenreInner.innerText = jsonData[index].genre.toUpperCase();
                    break;
                case 'javascript':
                    this.articleGenreInner.innerText = 'JavaScript';
                    break;
                case 'work':
                    this.articleGenreInner.innerText = 'Work';
                    break;
                case 'arekore':
                    this.articleGenreInner.innerText = 'A RE KO RE';
                    break;
            }
            this.articleTagListItem.length = jsonData[index].tag.length;
            for (let i = 0, maxI = this.articleTagListItem.length; i < maxI; i++) {
                this.articleTagListItem[i] = DOC.createElement('li');
                this.articleTagListItem[i].innerText = jsonData[index].tag[i];
                this.articleTagListItem[i].className = 'list-item';
                this.articleTagList.appendChild(this.articleTagListItem[i]);
            }

            if (position === 'main') {
                this.appendMain(index);
            } else if (position === 'side') {
                this.appendSide();
            }
        }
        appendMain(index) {
            if (genre=== jsonData[index].genre) {
                mainTarget.appendChild(this.articleWrapper);
            } else if (genre === '') {
                mainTarget.appendChild(this.articleWrapper);
            }
        }
        appendSide() {
            this.articleDesc.remove();  // サイドナビには説明テキストは不要
            this.articleImg.classList.add('side-back');
            this.articleHdg.className = 'side-hdg03';
            sideTarget.appendChild(this.articleWrapper);
        }
    }

    allArticle.length = jsonData.length;
    sideArticle.length = 5;
    if (!mainTarget) {
        allArticle.length = 5;
    }
    for (let i = 0, maxI = allArticle.length; i < maxI; i++) {
        allArticle[i] = new Article();
        sideArticle[i] = new Article();
        if (mainTarget) {
            allArticle[i].createArticle(i, 'main');
        }
        if (i < 5) {
            sideArticle[i].createArticle(i, 'side');
        }
    }
};